import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  jenisPengguna($event){
    if($event.detail.value == "c"){
      document.getElementById("user_content").innerHTML = "" ;
    } else if($event.detail.value == "r"){
      var html_r = `
      <ion-item>
        <ion-input style="font-size:14px;" placeholder="Longitude" type="text"></ion-input>
      </ion-item>
      <ion-item>
        <ion-input style="font-size:14px;" placeholder="Latitude" type="text"></ion-input>
      </ion-item>
      `;
      document.getElementById("user_content").innerHTML = html_r ;
    }
  }

}
