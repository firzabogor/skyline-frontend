import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public username:string;
  public password:string;

  constructor() {

  }

  loginClick(){
    document.getElementById("buffer-login").style.visibility = "visible";
  }
}
