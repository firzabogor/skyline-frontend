import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PreferensiPage } from './preferensi.page';

describe('PreferensiPage', () => {
  let component: PreferensiPage;
  let fixture: ComponentFixture<PreferensiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreferensiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PreferensiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
